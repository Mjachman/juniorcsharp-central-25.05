﻿using System;
using Infrastructure;

namespace HighScore
{
    public class HighScoreManager : IHighScoreManager
    {
        private readonly Lazy<IScoreStrategy> _storageStrategy;
       
        public HighScoreManager(Lazy<IScoreStrategy> storageStrategy, IGamesProvider gamesProvider)
        {
            _storageStrategy = storageStrategy;

            gamesProvider.GameFinished2 += GamesProviderOnGameFinished2;
        }

        private void GamesProviderOnGameFinished2(object sender, GameFinishedEventArgs gameFinishedEventArgs)
        {
            if (gameFinishedEventArgs.IsWon)
            {
                Add(new HighscoreEntry(gameFinishedEventArgs.Name, gameFinishedEventArgs.Result));
            }
        }

        public HighscoreEntry[] List()
        {
           return  _storageStrategy.Value.List();
        }

        public void Add(HighscoreEntry entry)
        {
            _storageStrategy.Value.Add(entry);
        }

    }
}
