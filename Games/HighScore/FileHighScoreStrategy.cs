﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Infrastructure;

namespace HighScore
{
    public class FileHighScoreStrategy : IScoreStrategy
    {
        private readonly string _filePath;
        private readonly int _howMany;

        public FileHighScoreStrategy(string filepath, int howmany)
        {
            _filePath = filepath;
            _howMany = howmany;
        }

        public HighscoreEntry[] List()
        {
            using (var highScoreFile = new HighScoreFile(_filePath))
               return highScoreFile.ReadEntries().OrderBy(a => a.Score).Take(_howMany).ToArray() ;
        }

        public void Add(HighscoreEntry entry)
        {
            using (var highScoreFile = new HighScoreFile(_filePath))
                highScoreFile.Append(entry);
        }

    }
}
