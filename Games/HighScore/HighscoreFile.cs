﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Infrastructure;

namespace HighScore
{
    public class HighScoreFile : IDisposable
    {
        private readonly FileStream _stream;

        public HighScoreFile(string path)
        {
            _stream = File.Open(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
        }

        public List<HighscoreEntry> ReadEntries()
        {
            _stream.Seek(0, SeekOrigin.Begin);

            var reader = new StreamReader(_stream);
            string allText = reader.ReadToEnd();
            var lines = allText.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            List<HighscoreEntry> divided = new List<HighscoreEntry>();
            int counter = 0;
            foreach (var line in lines)
            {
                counter++;
                if (counter == lines.Length)
                    break;
                var components = line.Split('-');
                divided.Add(new HighscoreEntry(components[0], int.Parse(components[1])));
                
            }
            return divided;
        }

        public void Append(HighscoreEntry entry)
        {
            _stream.Seek(0, SeekOrigin.End);

            using (var writer = new StreamWriter(_stream, Encoding.Default, 1024, true))
            {
                writer.WriteLine($"{entry.Name}-{entry.Score}");
            }
        }

        public void Dispose()
        {
            _stream.Dispose();
        }
    }
}
