﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure;

namespace GameLibrary
{
    public class GuessGameFactory :AbstractGameFactory
    {
        public override IGame CreateGameInstance()
        {
            return new GuessGame();
        }

        public override string Name => "Guess game";
    }
}
