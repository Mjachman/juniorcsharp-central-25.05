using System;

namespace Infrastructure
{
    public interface IGame
    {
        event EventHandler<MessagesEventArgs> Messages;
        event EventHandler<GameFinishedEventArgs> GameFinished;
        bool Guess(string bet);
        int Result { get; }
        string Name { get; }
    }
}