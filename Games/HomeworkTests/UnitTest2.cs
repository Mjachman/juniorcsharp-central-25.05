﻿using System;
using HighScore;
using Infrastructure;
using Moq;
using NUnit.Framework;

namespace HomeworkTests
{
    [TestFixture]
    public class UnitTest2
    {
        private class MockGameProvider : IGamesProvider
        {
            public event EventHandler<MessagesEventArgs> Messages2;
            public event EventHandler<GameFinishedEventArgs> GameFinished2
            {
                add { IsSubscribed = true; }
                remove
                {
                }
            }
            public bool IsSubscribed { get; private set; }

            public string[] GetGameNames()
            {
                throw new NotImplementedException();
            }

            public void ChooseGame(string name)
            {
                throw new NotImplementedException();
            }

            public void InitiateNextEvent(object oSender, MessagesEventArgs oEventArgs)
            {
                throw new NotImplementedException();
            }

            public bool Guess(string bet)
            {
                throw new NotImplementedException();
            }

            public int Result { get; }
            public string Name { get; set; }
        }
        [Test]
        public void Ctor_GameFlow_AttachedToGameFinishedEvent()
        {
            var provider  = new MockGameProvider();

            var manager = new HighScoreManager(new Lazy<IScoreStrategy>(()=>null), provider);

            Assert.That(()=>provider.IsSubscribed);
        }

        [Test]
        public void EventSubscriptions()
        {
            var provider = new Mock<IGamesProvider>();
            var strategy = new Mock<IScoreStrategy>();
            var manager = new HighScoreManager(new Lazy<IScoreStrategy>(() => strategy.Object), provider.Object);

            provider.Raise(gamesProvider => gamesProvider.GameFinished2 += null, provider.Object, new GameFinishedEventArgs(true, 12){Name="asd"});

            strategy.Verify(s=>s.Add(new HighscoreEntry("asd", 12)), Times.Once);
        }
    }
}
