﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace PrimeSearch.UI
{
    /// <summary>
    /// ViewModel for main window
    /// </summary>
    class MainViewModel : NotifyPropertyChanged
    {
        private uint _numberRange;
        private int _progress;
        private string _resultText;

        /// <summary>
        /// Default constructor
        /// </summary>
        public MainViewModel()
        {
            StartCommand = new DelegateCommand(Start);
        }

        /// <summary>
        /// Performs prime number search
        /// </summary>
        private void Start()
        {
            // todo start
        }

        /// <summary>
        /// Max Value, where prime numbers should be searched for 
        /// </summary>
        public uint NumberRange
        {
            get => _numberRange;
            set
            {
                _numberRange = value; 
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Progress from 0 to 100 of current operation
        /// </summary>
        public int Progress
        {
            get => _progress;
            set
            {
                _progress = value; 
                OnPropertyChanged();
            }
        }

        public ObservableCollection<int> PrimeNumbers { get; } = new ObservableCollection<int>(Enumerable.Range(1,50000000));

        public ICommand StartCommand { get; }

        public string ResultText
        {
            get => _resultText;
            set
            {
                _resultText = value; 
                OnPropertyChanged();
            }
        }
    }
}
